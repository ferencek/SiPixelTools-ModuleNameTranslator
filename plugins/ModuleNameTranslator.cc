// -*- C++ -*-
//
// Package:    SiPixelTools/ModuleNameTranslator
// Class:      ModuleNameTranslator
//
/**\class ModuleNameTranslator ModuleNameTranslator.cc SiPixelTools/ModuleNameTranslator/plugins/ModuleNameTranslator.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Dinko Ferencek
//         Created:  Thu, 02 Mar 2023 22:53:54 GMT
//
//

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/TrackerCommon/interface/PixelBarrelName.h"
#include "DataFormats/TrackerCommon/interface/TrackerTopology.h"
#include "Geometry/Records/interface/TrackerTopologyRcd.h"
#include "Geometry/Records/interface/TrackerDigiGeometryRecord.h"
#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"
#include "CondFormats/DataRecord/interface/SiPixelFedCablingMapRcd.h"
#include "CondFormats/SiPixelObjects/interface/SiPixelFedCablingMap.h"
#include "DQM/SiPixelPhase1Common/interface/SiPixelCoordinates.h"

//
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.

using reco::TrackCollection;

class ModuleNameTranslator : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
  explicit ModuleNameTranslator(const edm::ParameterSet&);
  ~ModuleNameTranslator() override;

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginJob() override;
  void analyze(const edm::Event&, const edm::EventSetup&) override;
  void endJob() override;

  // ----------member data ---------------------------
  edm::ESGetToken<TrackerTopology, TrackerTopologyRcd> topoEsToken_;
  edm::ESGetToken<TrackerGeometry, TrackerDigiGeometryRecord> geomEsToken_;
  edm::ESGetToken<SiPixelFedCablingMap, SiPixelFedCablingMapRcd> mapEsToken_;
  
  SiPixelCoordinates pixelCoords_;
  
  std::vector<std::string> moduleList_;
  std::vector<uint32_t> detIdList_;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
ModuleNameTranslator::ModuleNameTranslator(const edm::ParameterSet& iConfig)
    : topoEsToken_(esConsumes()),
      geomEsToken_(esConsumes()),
      mapEsToken_(esConsumes()),
      pixelCoords_(1),
      moduleList_(iConfig.getParameter<std::vector<std::string>>("moduleList")),
      detIdList_(iConfig.getParameter<std::vector<uint32_t>>("detIdList"))
{
  //now do what ever initialization is needed

}

ModuleNameTranslator::~ModuleNameTranslator() {
  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called for each event  ------------
void ModuleNameTranslator::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;

  const TrackerTopology* tTopo = &iSetup.getData(topoEsToken_);
  const TrackerGeometry* tGeom = &iSetup.getData(geomEsToken_);
  const SiPixelFedCablingMap* cMap = &iSetup.getData(mapEsToken_);

  pixelCoords_.init(tTopo, tGeom, cMap);
  
  if (!moduleList_.empty()) {
    std::cout << std::endl;
    for (auto const& modulename : moduleList_) {
      if (modulename.find("BPix_") != std::string::npos) {
        PixelBarrelName bn(modulename, true);
        const auto& detId = bn.getDetId(tTopo);
        const auto rawId = detId.rawId();
        std::cout << "Module: " << modulename << " --> " << "DetId: " << rawId << std::endl;
      }
    }
  }
  
  if (!detIdList_.empty()) {
    std::cout << std::endl;
    for (auto const& detid : detIdList_) {
        PixelBarrelName bn(detid, tTopo, true);
        const auto module = bn.name();
        std::cout << "DetId: " << detid << " --> " << "Module: " << module << " Signed ladder: " << pixelCoords_.signed_ladder(DetId(detid)) << std::endl;
    }
  }
}

// ------------ method called once each job just before starting event loop  ------------
void ModuleNameTranslator::beginJob() {
  // please remove this method if not needed
}

// ------------ method called once each job just after ending the event loop  ------------
void ModuleNameTranslator::endJob() {
  // please remove this method if not needed
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void ModuleNameTranslator::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  //descriptions.addWithDefaultLabel(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(ModuleNameTranslator);
