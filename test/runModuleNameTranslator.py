import FWCore.ParameterSet.Config as cms

process = cms.Process("USER")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic', '')

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1) )

process.source = cms.Source("EmptySource")

process.mnt = cms.EDAnalyzer('ModuleNameTranslator',
  moduleList = cms.vstring(
      "BPix_BpO_SEC1_LYR1_LDR1F_MOD3",
      "BPix_BpO_SEC4_LYR1_LDR3F_MOD3",
      "BPix_BmO_SEC2_LYR1_LDR1F_MOD1",
      "BPix_BmO_SEC1_LYR1_LDR1F_MOD3",
      "BPix_BmO_SEC4_LYR1_LDR3F_MOD1",
      "BPix_BmO_SEC7_LYR1_LDR5F_MOD1",
      "BPix_BmI_SEC3_LYR1_LDR3F_MOD4",
      "BPix_BpI_SEC3_LYR1_LDR3F_MOD4"
  ),
  detIdList= cms.vuint32(
      303042588,
      303050756,
      303054852,
      303079440,
      303075348
  )
)

process.p = cms.Path(process.mnt)

