# SiPixelTools-ModuleNameTranslator

```
cmsrel CMSSW_12_4_10
cd CMSSW_12_4_10/src
cmsenv
git clone https://gitlab.cern.ch/ferencek/SiPixelTools-ModuleNameTranslator.git SiPixelTools/ModuleNameTranslator
cd SiPixelTools/ModuleNameTranslator
scram b -j4
cmsRun test/runModuleNameTranslator.py
```
